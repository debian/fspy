fspy (0.1.1-4) unstable; urgency=medium

  * QA upload.
  * debian/control:
      - Restrict building to linux-any,
        since inotify is not available in non-linux platforms.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Sun, 07 Jun 2020 22:39:35 +0000

fspy (0.1.1-3) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Added the VCS fields to use Salsa.
      - Bumped Standards Version to 4.5.0.
  * debian/copyright:
      - Added my name and email address.
  * debian/patches/03_fix_makefile_cppflags.patch:
      - Fixed Makefile by adding CPPFLAGS.
  * debian/rules:
      - Added DEB_BUILD_MAINT_OPTIONS variable to improve GCC hardening.
  * debian/salsa-ci.yml:
      - Added to provide CI tests for Salsa.
  * debian/tests/control:
      - Created to perform a trivial CI test.
      - Added superficial Restriction.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Thu, 04 Jun 2020 02:59:13 +0000

fspy (0.1.1-2) unstable; urgency=medium

  * QA upload.
  * Applied reproducible build patch from Chris Lamb (Closes: #831354)
  * Bumped compat level to 10
  * Set maintainer to Debian QA Group
  * Dropped build-dep on quilt
  * Removed obsolete fields from d/control
  * Converted to source format 3.0 (quilt)
  * Converted d/rules to dh_ format
  * Removed useless watch file
  * Converted d/copyright to machine-readable format
  * Standards version 4.0.0
  * Renamed d/fspy.8 to d/fspy.1

 -- David William Richmond Davies-Jones <david@exultantmonkey.co.uk>  Tue, 01 Aug 2017 20:53:52 +0100

fspy (0.1.1-1) unstable; urgency=low

  * New Upstream Version
    + Upstream added the relevant LICENSE file and a copy of the GPL v2 and 3
      to the package, thanks!

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Sat, 31 Jan 2009 18:31:13 +0100

fspy (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #513577) (LP: #323054)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Fri, 30 Jan 2009 13:43:35 +0100
